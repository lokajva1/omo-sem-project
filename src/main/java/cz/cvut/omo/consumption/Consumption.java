package cz.cvut.omo.consumption;

import lombok.Getter;

@Getter
public class Consumption {
    private final int active;
    private final int idle;
    private final int off;
    public Consumption(int active, int idle, int off) {
        if (active < 0 || idle < 0 || off < 0) {
            throw new IllegalArgumentException();
        }
        this.active = active;
        this.idle = idle;
        this.off = off;
    }
}
