package cz.cvut.omo.applianceWithContent;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.applianceWithContent.models.Content;
import cz.cvut.omo.applianceWithContent.states.AbstractApplianceWithContentState;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * state pattern
 * expands Appliance with context of items that can be inserted or ejected
 */
@Getter
@Setter
@SuperBuilder
public abstract class ApplianceWithContent extends Appliance<AbstractApplianceWithContentState> {
    protected List<Content> content;

    public void insertContent(List<Content> content) {
        state.insertContent(this, content);
    }

    public List<Content> removeContent() {
        return state.removeContent(this);
    }
}
