package cz.cvut.omo.applianceWithContent.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class Content {
    private final String name;
}
