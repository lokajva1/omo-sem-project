package cz.cvut.omo.applianceWithContent.states;

import cz.cvut.omo.appliance.Appliance;

public class RunState extends AbstractApplianceWithContentState {
    @Override
    public void on(Appliance context) {
        super.on(context);
        context.setState(new OnState());
    }
}
