package cz.cvut.omo.applianceWithContent.states;

import cz.cvut.omo.applianceWithContent.ApplianceWithContent;
import cz.cvut.omo.applianceWithContent.models.Content;

import java.util.List;

public class OnState extends AbstractApplianceWithContentState {
    @Override
    public void insertContent(ApplianceWithContent context, List<Content> content) {
        super.insertContent(context, content);
        context.setContent(content);
        context.setState(new ReadyState());
    }

    @Override
    public List<Content> removeContent(ApplianceWithContent context) {
        super.removeContent(context);
        List<Content> content = context.getContent();
        context.getContent().clear();
        return content;
    }
}
