package cz.cvut.omo.applianceWithContent.states;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.applianceWithContent.ApplianceWithContent;
import cz.cvut.omo.applianceWithContent.ApplianceWithContentState;
import cz.cvut.omo.applianceWithContent.models.Content;

import java.util.List;

public abstract class AbstractApplianceWithContentState implements ApplianceWithContentState {

    @Override
    public void on(Appliance context) {
    }

    @Override
    public void off(Appliance context) {
        context.setState(new OffState());
    }

    @Override
    public void running(Appliance context) {
        checkFunctionality(context);
    }

    @Override
    public void error(Appliance context) {
    }

    @Override
    public void fix(Appliance context) {

    }

    @Override
    public void insertContent(ApplianceWithContent context, List<Content> content) {
    }

    @Override
    public List<Content> removeContent(ApplianceWithContent context) {
        return null;
    }

    protected void checkFunctionality(Appliance context) {
        if (!context.isOperational()) {
            context.setState(new ErrorState());
        }
    }
}
