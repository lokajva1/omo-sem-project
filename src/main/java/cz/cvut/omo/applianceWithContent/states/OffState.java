package cz.cvut.omo.applianceWithContent.states;

import cz.cvut.omo.appliance.Appliance;

public class OffState extends AbstractApplianceWithContentState {
    @Override
    public void on(Appliance context) {
        context.setState(new OnState());
    }
}
