package cz.cvut.omo.applianceWithContent.states;

import cz.cvut.omo.appliance.Appliance;

public class ReadyState extends AbstractApplianceWithContentState {
    @Override
    public void running(Appliance context) {
        super.running(context);
        context.setState(new RunState());
    }
}
