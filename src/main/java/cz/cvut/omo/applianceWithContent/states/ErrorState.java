package cz.cvut.omo.applianceWithContent.states;

import cz.cvut.omo.appliance.Appliance;

public class ErrorState extends AbstractApplianceWithContentState {
    @Override
    public void fix(Appliance context) {
        context.setFunctionality(100);
        context.setState(new OnState());
    }
}
