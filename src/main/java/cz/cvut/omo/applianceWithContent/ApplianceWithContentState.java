package cz.cvut.omo.applianceWithContent;

import cz.cvut.omo.appliance.ApplianceState;
import cz.cvut.omo.applianceWithContent.models.Content;

import java.util.List;

public interface ApplianceWithContentState extends ApplianceState {
    void insertContent(ApplianceWithContent context, List<Content> content);
    List<Content> removeContent(ApplianceWithContent context);
}
