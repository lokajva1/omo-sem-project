package cz.cvut.omo.event;

import java.util.Iterator;

public interface EventContainer {
    Iterator<Event> getIterator();
}
