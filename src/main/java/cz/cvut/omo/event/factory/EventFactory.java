package cz.cvut.omo.event.factory;

import cz.cvut.omo.event.Event;
import cz.cvut.omo.event.EventWithContext;
import cz.cvut.omo.exceptions.UnsupportedEventTypeException;

import java.util.List;
import java.util.Optional;

import static cz.cvut.omo.event.Event.EventType.*;

public class EventFactory {
    public Event crateEvent(String applianceId,int timeNeeded, int priority, String type) throws UnsupportedEventTypeException {
        return Event.builder()
                .applianceId(applianceId)
                .appliance(null)
                .timeNeeded(timeNeeded)
                .priority(priority)
                .eventType(resolveEventType(type))
                .build();
    }

    public EventWithContext crateEventWithContext(String applianceId, int timeNeeded, int priority, String type) throws UnsupportedEventTypeException {
        return EventWithContext.builder()
                .applianceId(applianceId)
                .appliance(null)
                .timeNeeded(timeNeeded)
                .priority(priority)
                .eventType(resolveEventWithContextType(type))
                .contents(List.of())
                .build();
    }

    private Event.EventType resolveEventType(String type) throws UnsupportedEventTypeException {
        Event.EventType eventType = Event.EventType.valueOf(type);
        if (!List.of(ON, USE, OFF, FIX)
                .contains(eventType)) {
            throw new UnsupportedEventTypeException();
        }
        return eventType;
    }

    private Event.EventType resolveEventWithContextType(String type) throws UnsupportedEventTypeException {
        return Optional.of(Event.EventType.valueOf(type))
                .orElseThrow(UnsupportedEventTypeException::new);
    }
}
