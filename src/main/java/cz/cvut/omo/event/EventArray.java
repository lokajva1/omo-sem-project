package cz.cvut.omo.event;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * iterator pattern
 * implements sorted array of events from the highest priority to lowest...
 */
public class EventArray implements EventContainer {
    private final List<Event> list;

    public EventArray() {
        list = new ArrayList<>();
    }

    @Override
    public Iterator<Event> getIterator() {
        return new EventIterator();
    }

    public void add(Event event) {
        int index = 0;
        for(Event e : list){
            if(e.getPriority() < event.getPriority()){
                break;
            }
            index++;
        }
        list.add(index, event);
    }

    public void removeFirstEvent() {
        list.remove(0);
    }

    private class EventIterator implements Iterator<Event> {
        int index = 0;

        @Override
        public boolean hasNext() {
            return index < list.size();
        }

        @Override
        public Event next() {
            if (hasNext()) {
                return list.get(index++);
            }
            return null;
        }
    }
}
