package cz.cvut.omo.event;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.states.ErrorState;
import cz.cvut.omo.common.AbstractEvent;
import cz.cvut.omo.exceptions.UnsupportedEventTypeException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * facade
 */
@SuperBuilder
@Getter
@Setter
public class Event extends AbstractEvent {

    protected String applianceId;
    protected Appliance appliance;

    private final int priority;

    protected final EventType eventType;

    public void doEvent() throws UnsupportedEventTypeException {
        switch (eventType){
            case ON -> turnApplianceOn();
            case USE -> useAppliance();
            case FIX -> fixAppliance();
            case OFF -> turnApplianceOff();
            default -> throw new UnsupportedEventTypeException();
        }
    }

    protected void useAppliance(){
        if (isConsumptionInErrorState()){
            fixAppliance();
            return;
        }
        appliance.run();
        if (isDoneAndIncrementTimeUsed()){
            appliance.on();
        }
    }

    protected void turnApplianceOn(){
        if (isConsumptionInErrorState()){
            fixAppliance();
            return;
        }
        if (isDoneAndIncrementTimeUsed()){
            appliance.on();
        }
    }

    protected void turnApplianceOff(){
        if (isDoneAndIncrementTimeUsed()){
            appliance.off();
        }
    }

    protected void fixAppliance(){
        if (!isConsumptionInErrorState()){
            timeUsed = timeNeeded;
        }
        if (isDoneAndIncrementTimeUsed()){
            appliance.fix();
        }
    }

    protected boolean isDoneAndIncrementTimeUsed(){
        timeUsed++;
        return isDone();
    }

    private boolean isConsumptionInErrorState(){
        if (appliance.getState().getClass() == ErrorState.class){
            appliance.error();
            return true;
        }
        return false;
    }

    public enum EventType {
        ON, OFF, USE, FIX, INSERT, EJECT
    }
}
