package cz.cvut.omo.event;

import cz.cvut.omo.applianceWithContent.models.Content;
import cz.cvut.omo.applianceWithContent.states.ErrorState;
import cz.cvut.omo.applianceWithContent.ApplianceWithContent;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
public class EventWithContext extends Event {

    private final List<Content> contents;

    public void doEvent() {
        switch (eventType){
            case ON -> turnApplianceOn();
            case USE -> useAppliance();
            case FIX -> fixAppliance();
            case OFF -> turnApplianceOff();
            case INSERT -> insert();
            case EJECT -> eject();
        }
    }

    public List<Content> ejectContents(){
        ApplianceWithContent applianceWithContent = (ApplianceWithContent) appliance;
        return applianceWithContent.removeContent();
    }

    private void insert(){
        if (isConsumptionInErrorState()){
            timeUsed = timeNeeded;
        }
        if (isDoneAndIncrementTimeUsed()){
            ApplianceWithContent applianceWithContent = (ApplianceWithContent) appliance;
            applianceWithContent.insertContent(contents);
        }
    }

    private void eject(){
        if (isConsumptionInErrorState()){
            timeUsed = timeNeeded;
        }
        if (isDoneAndIncrementTimeUsed()){
            ApplianceWithContent applianceWithContent = (ApplianceWithContent) appliance;
            contents.addAll(applianceWithContent.removeContent());
        }
    }

    private boolean isConsumptionInErrorState(){
        if (appliance.getState().getClass() == ErrorState.class){
            appliance.error();
            return true;
        }
        return false;
    }
}
