package cz.cvut.omo.persons;

import cz.cvut.omo.activity.Activity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Person extends Being {
    private final Queue<Activity> activities = new LinkedList<>();

    private boolean finishedActivity;

    public Activity getFirstActivity(){
        return activities.peek();
    }

    public void clearDoneActivities(){
        Activity activity = activities.peek();
        if (activity != null && activity.isDone()) {
            finishedActivity = true;
            activities.poll();
        }
    }
    public void addActivity(Collection<Activity> arr){
        Optional.ofNullable(arr)
                        .ifPresent(activities::addAll);
    }
    public boolean finishedActivity(){
        return finishedActivity;
    }
    public boolean hasSomeActivity(){
        return !activities.isEmpty();
    }
}
