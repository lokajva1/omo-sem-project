package cz.cvut.omo.persons.factory;

import cz.cvut.omo.persons.Animal;
import cz.cvut.omo.persons.Person;
import cz.cvut.omo.persons.sex.Female;
import cz.cvut.omo.persons.sex.Male;

public class BeingFactory {
    public Person createPerson(String id, String name, String sexJson){
        return Person.builder()
                .id(id)
                .name(name)
                .sex(sexJson.equals("M") ? new Male() : new Female())
                .usedAppliance(null)
                .finishedActivity(false)
                .build();
    }

    public Animal createAnimal(String id, String name, String sexJson, String kind){
        return Animal.builder()
                .id(id)
                .name(name)
                .sex(sexJson.equals("M") ? new Male() : new Female())
                .usedAppliance(null)
                .kind(Animal.KindOfAnimal.valueOf(kind))
                .build();
    }
}
