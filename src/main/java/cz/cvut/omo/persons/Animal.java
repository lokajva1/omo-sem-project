package cz.cvut.omo.persons;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public class Animal extends Being {
    private KindOfAnimal kind;

    public enum KindOfAnimal {
        CAT, DOG
    }
}
