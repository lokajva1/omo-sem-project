package cz.cvut.omo.persons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.event.Event;
import cz.cvut.omo.event.EventArray;
import cz.cvut.omo.event.EventWithContext;
import cz.cvut.omo.logger.AbstractLogger;
import cz.cvut.omo.logger.Logger;
import cz.cvut.omo.persons.sex.Sex;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * bridge
 */
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public abstract class Being {
    private String id;
    private String name;

    @JsonIgnore
    private Sex sex;

    @JsonIgnore
    private final EventArray eventArray = new EventArray();

    @JsonIgnore
    private Appliance usedAppliance;

    public void addEvent(List<Event> arr) {
        Optional.ofNullable(arr)
                .ifPresent(events -> events.forEach(eventArray::add));
    }

    public void addEventWithContext(Collection<EventWithContext> arr) {
        Optional.ofNullable(arr)
                .ifPresent(events -> events.forEach(eventArray::add));
    }

    public void clearFirstEvent() {
        usedAppliance.setUsed(false);
        usedAppliance = null;
        eventArray.removeFirstEvent();
    }

    public Event getFirstAvailableEvent() {
        for (Iterator<Event> it = eventArray.getIterator(); it.hasNext(); ) {
            Event event = it.next();
            if (event.getAppliance() == null){
                Logger.getLogger().logMessage(AbstractLogger.WARNING, "zombie event that do not have appliance");
                continue;
            }
            if (!event.getAppliance().isUsed() || event.getAppliance() == usedAppliance) {
                return event;
            }
        }
        return null;
    }

    public boolean isUsingAppliance() {
        return usedAppliance != null;
    }

    public void setUsedAppliance(Appliance appliance) {
        usedAppliance = appliance;
        usedAppliance.setUsed(true);
    }
}

