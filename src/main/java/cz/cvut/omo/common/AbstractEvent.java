package cz.cvut.omo.common;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public class AbstractEvent {
    protected final int timeNeeded;
    protected int timeUsed;
    public final boolean isDone() {
        return timeUsed >= timeNeeded;
    }
}
