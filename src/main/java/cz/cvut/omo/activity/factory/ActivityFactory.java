package cz.cvut.omo.activity.factory;

import cz.cvut.omo.activity.Activity;
import cz.cvut.omo.activity.activities.Biking;
import cz.cvut.omo.activity.activities.Skiing;
import cz.cvut.omo.exceptions.UnknownActivityType;

public class ActivityFactory {
    public Activity createActivity( String type, int timeNeeded) throws UnknownActivityType {
        return switch (type){
            case "SKI" -> Skiing
                    .builder()
                    .timeNeeded(timeNeeded)
                    .timeUsed(0)
                    .build();
            case "BIKE" -> Biking
                    .builder()
                    .timeNeeded(timeNeeded)
                    .timeUsed(0)
                    .build();
            default -> throw new UnknownActivityType();
        };
    }
}
