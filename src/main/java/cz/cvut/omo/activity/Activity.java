package cz.cvut.omo.activity;

import cz.cvut.omo.activity.models.Equipment;
import cz.cvut.omo.common.AbstractEvent;
import cz.cvut.omo.logger.AbstractLogger;
import cz.cvut.omo.logger.Logger;
import cz.cvut.omo.persons.Being;
import lombok.Getter;
import cz.cvut.omo.exceptions.NoEquipmentAvailableException;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


/**
 * template
 */
@Getter
@Setter
@SuperBuilder
public abstract class Activity extends AbstractEvent {
    protected Equipment equipment;

    protected abstract Equipment findAvailableEquipment() throws NoEquipmentAvailableException;

    protected abstract void useSportEquipment();

    public final void doSport(Being being) throws NoEquipmentAvailableException {
        if (timeUsed == 0) {
            equipment = findAvailableEquipment();
            equipment.setUsed(true);
            Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + being.getName() + " begin use " + equipment.getType());
        }
        useSportEquipment();
        Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + being.getName() + " use " + equipment.getType());
        if (isDone()) {
            Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + being.getName() + " finish using of " + equipment.getType());
            equipment.setUsed(false);
        }
    }
}
