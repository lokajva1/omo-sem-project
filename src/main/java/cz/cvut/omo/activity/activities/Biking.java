package cz.cvut.omo.activity.activities;

import cz.cvut.omo.activity.Activity;
import cz.cvut.omo.activity.models.Equipment;
import cz.cvut.omo.house.House;
import cz.cvut.omo.exceptions.NoEquipmentAvailableException;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class Biking extends Activity {
    @Override
    protected Equipment findAvailableEquipment() throws NoEquipmentAvailableException {
        Equipment foundEquipment = House.getInstance()
                .getOutdoor()
                .findEquipment(Equipment.EquipmentType.BIKE);
        if (foundEquipment == null) {
            throw new NoEquipmentAvailableException();
        }
        return foundEquipment;
    }

    @Override
    protected void useSportEquipment() {
        timeUsed++;
    }
}
