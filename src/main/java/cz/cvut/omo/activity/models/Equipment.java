package cz.cvut.omo.activity.models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Equipment {
    private final EquipmentType type;
    private boolean isUsed;

    public Equipment(EquipmentType type) {
        this.type = type;
        isUsed = false;
    }

    public enum EquipmentType {
        SKI, BIKE
    }
}
