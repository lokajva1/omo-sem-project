package cz.cvut.omo.loader.manual;

import java.nio.charset.StandardCharsets;
import java.util.Random;

/**
 * lazy loading
 */

public class ApplianceManualLoader {

    private String text;

    public String getText(){
        if (text == null){
//            generate random string of 666 chars (instead of load it from external source)
            byte[] array = new byte[666];
            new Random().nextBytes(array);
            text = new String(array, StandardCharsets.UTF_8);
        }
        return text;
    }
}
