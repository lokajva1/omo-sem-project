package cz.cvut.omo.loader.house;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.omo.activity.Activity;
import cz.cvut.omo.activity.factory.ActivityFactory;
import cz.cvut.omo.activity.models.Equipment;
import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.factory.ApplianceFactory;
import cz.cvut.omo.event.Event;
import cz.cvut.omo.event.EventWithContext;
import cz.cvut.omo.event.factory.EventFactory;
import cz.cvut.omo.exceptions.UnknownActivityType;
import cz.cvut.omo.exceptions.UnknownApplianceType;
import cz.cvut.omo.exceptions.UnsupportedEventTypeException;
import cz.cvut.omo.house.factory.HouseFactory;
import cz.cvut.omo.house.room.Room;
import cz.cvut.omo.house.window.Window;
import cz.cvut.omo.logger.AbstractLogger;
import cz.cvut.omo.logger.Logger;
import cz.cvut.omo.persons.Being;
import cz.cvut.omo.persons.factory.BeingFactory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HouseLoader {

    public List<Being> loadPersonsFromFile(String path) {
        List<LinkedHashMap<String, String>> personsJson = loadFileJsonObjects(path);
        BeingFactory personFactory = new BeingFactory();
        return personsJson.stream()
                .map(personJson -> personFactory.createPerson(personJson.get("id"), personJson.get("name"), personJson.get("sex")))
                .collect(Collectors.toList());
    }

    public List<Being> loadAnimasFromFile(String path) {
        List<LinkedHashMap<String, String>> animalsJson = loadFileJsonObjects(path);
        BeingFactory personFactory = new BeingFactory();
        return animalsJson.stream()
                .map(animalJson -> personFactory.createAnimal(animalJson.get("id"), animalJson.get("name"), animalJson.get("sex"), animalJson.get("kind")))
                .collect(Collectors.toList());
    }

    public List<Equipment> loadEquipments(String path) {
        List<LinkedHashMap<String, String>> equipmentsJson = loadFileJsonObjects(path);
        return equipmentsJson.stream()
                .map(equipmentJson -> new Equipment(Equipment.EquipmentType.valueOf(equipmentJson.get("type"))))
                .collect(Collectors.toList());
    }

    public List<Room> loadRooms(String path) {
        List<LinkedHashMap<String, String>> roomsJson = loadFileJsonObjects(path);
        HouseFactory houseFactory = new HouseFactory();
        return roomsJson.stream()
                .map(roomJson -> houseFactory.createRoom(roomJson.get("id")))
                .collect(Collectors.toList());
    }

    public Map<String, List<Appliance>> loadAppliances(String path) {
        List<LinkedHashMap<String, String>> appliancesJson = loadFileJsonObjects(path);
        ApplianceFactory applianceFactory = new ApplianceFactory();
        Map<String, List<Appliance>> applianceWithRooms = new HashMap<>();
        appliancesJson
                .forEach(applianceJson -> {
                    try {
                        Appliance appliance = applianceFactory.createAppliance(applianceJson.get("type"), applianceJson.get("id"), applianceJson.get("name"), applianceJson.get("strategy"),
                                Integer.parseInt(applianceJson.get("active")), Integer.parseInt(applianceJson.get("idle")), Integer.parseInt(applianceJson.get("off")));
                        String roomId = applianceJson.get("room_id");
                        applianceWithRooms.computeIfAbsent(roomId, k -> new ArrayList<>());
                        applianceWithRooms.get(roomId).add(appliance);
                    } catch (UnknownApplianceType e) {
                       Logger.getLogger().logMessage(AbstractLogger.WARNING, "could not load appliance from file");
                    }
                });
        return applianceWithRooms;
    }

    public Map<String, List<Activity>> loadActivities(String path) {
        List<LinkedHashMap<String, String>> activitiesJson = loadFileJsonObjects(path);
        ActivityFactory activityFactory = new ActivityFactory();
        Map<String, List<Activity>> activitiesWithBeing = new HashMap<>();
        activitiesJson
                .forEach(activityJson -> {
                    try {
                        Activity activity = activityFactory.createActivity(activityJson.get("type"), Integer.parseInt(activityJson.get("time_needed")));
                        String personId = activityJson.get("person_id");
                        activitiesWithBeing.computeIfAbsent(personId, k -> new ArrayList<>());
                        activitiesWithBeing.get(personId).add(activity);
                    } catch (UnknownActivityType e) {
                        Logger.getLogger().logMessage(AbstractLogger.WARNING, "could not load activity from file");
                    }
                });
        return activitiesWithBeing;
    }

    public Map<String, List<Event>> loadEvents(String path) {
        List<LinkedHashMap<String, String>> eventsJson = loadFileJsonObjects(path);
        EventFactory eventFactory = new EventFactory();
        Map<String, List<Event>> eventsWithBeing = new HashMap<>();
        eventsJson
                .forEach(eventJson -> {
                    try {
                        Event event = eventFactory.crateEvent(eventJson.get("appliance_id"), Integer.parseInt(eventJson.get("time_needed")), Integer.parseInt(eventJson.get("priority")), eventJson.get("type"));
                        String personId = eventJson.get("being_id");
                        eventsWithBeing.computeIfAbsent(personId, k -> new ArrayList<>());
                        eventsWithBeing.get(personId).add(event);
                    } catch (UnsupportedEventTypeException e) {
                        Logger.getLogger().logMessage(AbstractLogger.WARNING, "could not load event from file");
                    }
                });
        return eventsWithBeing;
    }

    public Map<String, List<EventWithContext>> loadEventsWithContext(String path) {
        List<LinkedHashMap<String, String>> eventWithContextWithBeingJson = loadFileJsonObjects(path);
        EventFactory eventFactory = new EventFactory();
        Map<String, List<EventWithContext>> eventWithContextWithBeing = new HashMap<>();
        eventWithContextWithBeingJson
                .forEach(eventWCWBJson -> {
                    try {
                        EventWithContext event = eventFactory.crateEventWithContext(eventWCWBJson.get("appliance_id"), Integer.parseInt(eventWCWBJson.get("time_needed")), Integer.parseInt(eventWCWBJson.get("priority")), eventWCWBJson.get("type"));
                        String personId = eventWCWBJson.get("being_id");
                        eventWithContextWithBeing.computeIfAbsent(personId, k -> new ArrayList<>());
                        eventWithContextWithBeing.get(personId).add(event);
                    } catch (UnsupportedEventTypeException e) {
                        Logger.getLogger().logMessage(AbstractLogger.WARNING, "could not load event with context from file");
                    }
                });
        return eventWithContextWithBeing;
    }

    public Map<String, List<Window>> loadWindows(String path) {
        List<LinkedHashMap<String, String>> windowsWithRoomJson = loadFileJsonObjects(path);
        Map<String, List<Window>> windowsWithRoom = new HashMap<>();
        windowsWithRoomJson
                .forEach(eventWCWBJson -> {
                    Window window = new Window();
                    String roomId = eventWCWBJson.get("room_id");
                    windowsWithRoom.computeIfAbsent(roomId, k -> new ArrayList<>());
                    windowsWithRoom.get(roomId).add(window);
                });
        return windowsWithRoom;
    }

    private List<LinkedHashMap<String, String>> loadFileJsonObjects(String path) {
        List<LinkedHashMap<String, String>> jsonObjects = new ArrayList<>();
        try {
            String jsonInput = new String(Files.readAllBytes(Paths.get(path)));
            ObjectMapper mapper = new ObjectMapper();
            jsonObjects = mapper.readValue(jsonInput, new TypeReference<>() {
            });
        } catch (Exception e) {
            Logger.getLogger().logMessage(AbstractLogger.WARNING, "could not parse file from json");
        }
        return jsonObjects;
    }
}
