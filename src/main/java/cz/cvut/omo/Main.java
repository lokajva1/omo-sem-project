package cz.cvut.omo;


import cz.cvut.omo.house.House;
import cz.cvut.omo.logger.AbstractLogger;
import cz.cvut.omo.logger.Logger;


public class Main {
    public static void main(String[] args) {
        try {
            House.getInstance().loadHouseConfig();
        } catch (Exception e){
            Logger.getLogger().logMessage(AbstractLogger.ERROR, "error while loading config from file");
           return;
        }
//        time-cycle of house
        for (int i = 0; i < 20; i++) {
            House.getInstance().doTimeCycle();
        }
//        generate report
        House.getInstance().generateReport();
    }
}