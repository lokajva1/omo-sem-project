package cz.cvut.omo.logger;

/**
 * singleton
 */
public class Logger {
    private static Logger logger;
    private static AbstractLogger infoLogger;

    private Logger() {
        infoLogger = new InfoLogger();
        AbstractLogger warningLogger = new WarningLogger();
        AbstractLogger errorLogger = new ErrorLogger();

        infoLogger.setNextLogger(warningLogger);
        warningLogger.setNextLogger(errorLogger);
    }

    public static AbstractLogger getLogger(){
        if (logger == null){
            logger = new Logger();
        }
        return infoLogger;
    }
}
