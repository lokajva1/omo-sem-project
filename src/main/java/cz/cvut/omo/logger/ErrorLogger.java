package cz.cvut.omo.logger;


public class ErrorLogger extends AbstractLogger{
    public ErrorLogger(){
        this.level = AbstractLogger.ERROR;
    }
    @Override
    protected void write(String message) {
        System.out.println("ERROR::Logger: " + message);
        logToFile("ERROR::Logger: " + message);
    }
}
