package cz.cvut.omo.logger;

public class InfoLogger extends AbstractLogger{
    public InfoLogger() {
        this.level = AbstractLogger.INFO;
    }

    @Override
    protected void write(String message) {
        System.out.println("INFO::Logger: " + message);
        logToFile("INFO::Logger: " + message);
    }
}
