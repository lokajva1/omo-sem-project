package cz.cvut.omo.logger;

public class WarningLogger extends AbstractLogger{
    public WarningLogger(){
        this.level = AbstractLogger.WARNING;
    }
    @Override
    protected void write(String message) {
        logToFile("WARING::Logger: " + message);
    }
}