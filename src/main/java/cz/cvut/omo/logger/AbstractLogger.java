package cz.cvut.omo.logger;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

/**
 * chain of responsibility
 */
public abstract class AbstractLogger {
    public static final int INFO = 1;
    public static final int WARNING = 2;
    public static final int ERROR = 3;
    protected int level;

    protected AbstractLogger nextLogger;

    private String path;

    public void setNextLogger(AbstractLogger nextLogger) {
        this.nextLogger = nextLogger;
    }

    public void logMessage(int level, String message){
        if(this.level <= level){
            write(message);
        }
        if(nextLogger != null){
            nextLogger.logMessage(level, message);
        }
    }

    abstract protected void write(String message);

    protected void logToFile(String message){
        if (Objects.equals(path, "path_not_found")){
            return;
        }
        if (path == null){
            try {
                loadLoggerPath();
                BufferedWriter writer = new BufferedWriter(new FileWriter(path));
                writer.close();
            }catch (Exception e){
                path = "path_not_found";
            }
        }
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(path, true));
            writer.write(message);
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private void loadLoggerPath() throws IOException {
        InputStream input = new FileInputStream("src/main/resources/config.properties");
        Properties prop = new Properties();
        prop.load(input);
        path = prop.getProperty("logger.path");
    }
}
