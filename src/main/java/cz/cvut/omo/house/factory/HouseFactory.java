package cz.cvut.omo.house.factory;

import cz.cvut.omo.house.room.Room;

import java.util.ArrayList;

public class HouseFactory {
    public Room createRoom(String id){
        return Room.builder()
                .id(id)
                .appliances(new ArrayList<>())
                .beings(new ArrayList<>())
                .windows(new ArrayList<>())
                .build();
    }
}
