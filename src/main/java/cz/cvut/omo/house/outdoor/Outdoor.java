package cz.cvut.omo.house.outdoor;

import cz.cvut.omo.activity.models.Equipment;
import cz.cvut.omo.persons.Being;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * builder
 */

@Getter
public class Outdoor {

    private final List<Being> beings;

    private final List<Equipment> equipments;

    public Outdoor() {
        beings = new ArrayList<>();
        equipments = new ArrayList<>();
    }

    public Equipment findEquipment(Equipment.EquipmentType equipmentType) {
        return equipments.stream()
                .filter(equipment -> equipment.getType() == equipmentType && !equipment.isUsed())
                .findFirst()
                .orElse(null);
    }

    public void addEquipments(Collection<Equipment> arr){
        Optional.ofNullable(arr)
                .ifPresent(equipments::addAll);
    }

    public void addBeings(Collection<Being> arr){
        Optional.ofNullable(arr)
                .ifPresent(beings::addAll);
    }

    public void removeBeings(Collection<Being> arr){
        beings.removeAll(arr);
    }
}
