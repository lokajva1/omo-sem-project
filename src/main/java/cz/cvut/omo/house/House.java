package cz.cvut.omo.house;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.omo.activity.Activity;
import cz.cvut.omo.activity.models.Equipment;
import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;
import cz.cvut.omo.event.Event;
import cz.cvut.omo.event.EventWithContext;
import cz.cvut.omo.exceptions.NoEquipmentAvailableException;
import cz.cvut.omo.exceptions.UnsupportedEventTypeException;
import cz.cvut.omo.exporter.HouseExporter;
import cz.cvut.omo.house.outdoor.Outdoor;
import cz.cvut.omo.house.room.Room;
import cz.cvut.omo.house.window.Window;
import cz.cvut.omo.loader.house.HouseLoader;
import cz.cvut.omo.logger.AbstractLogger;
import cz.cvut.omo.logger.Logger;
import cz.cvut.omo.persons.Being;
import cz.cvut.omo.persons.Person;
import lombok.Getter;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * singleton
 */

@Getter
public class House {
    private int timeCycle;
    private List<Room> rooms;
    private final Outdoor outdoor;
    @JsonIgnore
    private static House instance;
    private String dataPath;
    private String exportPath;

    private House() {
        timeCycle = 0;
        outdoor = new Outdoor();
    }

    public static House getInstance() {
        if (instance == null) {
            instance = new House();
        }
        return instance;
    }

    public void loadHouseConfig() throws Exception {
        loadProperties();

        HouseLoader houseLoader = new HouseLoader();
        List<Room> roomList = houseLoader.loadRooms(dataPath + "/rooms.json");
        List<Equipment> equipmentList = houseLoader.loadEquipments(dataPath + "/equipments.json");
        List<Being> animalList = houseLoader.loadAnimasFromFile(dataPath + "/animals.json");
        List<Being> personList = houseLoader.loadPersonsFromFile(dataPath + "/persons.json");
        Map<String, List<Window>> windowsWithRoom = houseLoader.loadWindows(dataPath + "/windows.json");
        Map<String, List<Activity>> activitiesMapByPerson = houseLoader.loadActivities(dataPath + "/activities.json");
        Map<String, List<Appliance>> appliancesMapByRooms = houseLoader.loadAppliances(dataPath + "/appliances.json");
        Map<String, List<Event>> eventsMapByBeing = houseLoader.loadEvents(dataPath + "/events.json");
        Map<String, List<EventWithContext>> eventWithContextMapByBeing = houseLoader.loadEventsWithContext(dataPath + "/eventsWithContext.json");

        rooms = roomList;
        outdoor.addEquipments(equipmentList);
        outdoor.addBeings(animalList);
        rooms.get(0).addBeings(personList);

        roomList.forEach(room -> room.addWindow(windowsWithRoom.get(room.getId())));
        personList.stream()
                .map(person -> (Person) person)
                .forEach(person -> person.addActivity(activitiesMapByPerson.get(person.getId())));
        roomList.forEach(room -> room.addAppliances(appliancesMapByRooms.get(room.getId())));

        Map<String, Appliance> applianceMap = rooms.stream()
                .map(Room::getAppliances)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(Appliance::getId, appliance -> appliance));

        eventsMapByBeing.forEach((beingId, events) -> events.forEach(
                event -> event.setAppliance(applianceMap.get(event.getApplianceId())))
        );

        eventWithContextMapByBeing.forEach((beingId, events) -> events.forEach(
                event -> event.setAppliance(applianceMap.get(event.getApplianceId())))
        );

        Stream.of(animalList, personList)
                .flatMap(Collection::stream)
                .forEach(being -> being.addEvent(eventsMapByBeing.get(being.getId())));
        Stream.of(animalList, personList)
                .flatMap(Collection::stream)
                .forEach(being -> being.addEventWithContext(eventWithContextMapByBeing.get(being.getId())));
    }

    public void doTimeCycle() {
        Logger.getLogger().logMessage(AbstractLogger.INFO, "Time cycle " + timeCycle);
//        activities cycle
        doTimeCycleOutdoorActivities();
//        appliance events
        doTimeCyclePersonsAppliance();
//        move persons
        movePersons();
//        update events and activities
        updateOnCycleEnd();
        timeCycle++;
    }

    public void generateReport() {
        try{
            HouseExporter houseExporter = new HouseExporter();
            houseExporter.exportApplianceConsumption(exportPath + "/report.json");
        }catch (Exception e){
           Logger.getLogger().logMessage(AbstractLogger.ERROR, "Failed generate report");
        }
    }

    private void loadProperties() throws Exception {
        InputStream input = new FileInputStream("src/main/resources/config.properties");
        Properties prop = new Properties();
        prop.load(input);
        dataPath = prop.getProperty("data.path");
        exportPath = prop.getProperty("export.path");
    }

    private void doTimeCycleOutdoorActivities() {
        List<Person> persons = outdoor.getBeings().stream()
                .filter(being -> being instanceof Person)
                .map(being -> (Person) being)
                .toList();
        for (Person person : persons) {
            Activity activity = person.getFirstActivity();
            if (activity == null) {
                continue;
            }
            try {
                person.getFirstActivity().doSport(person);
                person.clearDoneActivities();
            } catch (NoEquipmentAvailableException e) {
                Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + person.getName() + " not found equipment ");
            }
        }
    }

    private void doTimeCyclePersonsAppliance() {
        for (Room room : rooms) {
            useAllApplianceInRoom(room);
        }
    }

    private void movePersons() {
        List<Being> beingThatAlreadyMoved = new ArrayList<>();
        List<Being> beingToMove = new ArrayList<>();
        if (outdoor.getBeings().size() > 0) {
            beingToMove = outdoor.getBeings().stream()
                    .filter(this::moveOutside)
                    .toList();
            outdoor.removeBeings(beingToMove);
            beingThatAlreadyMoved.addAll(beingToMove);
            beingToMove.forEach(
                    person -> Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + person.getName() + " left outside")
            );
        }
        for (Room room : rooms) {
            if (room != rooms.get(0)) {
                beingToMove.forEach(
                        person -> Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + person.getName() + " move in house")
                );
            }
            room.addBeings(beingToMove);
            beingToMove = room.getBeings().stream()
                    .filter(person -> !person.isUsingAppliance())
                    .filter(person -> !beingThatAlreadyMoved.contains(person))
                    .toList();
            room.removePersons(beingToMove);
            beingThatAlreadyMoved.addAll(beingToMove);
        }
        if (beingToMove.size() > 0) {
            outdoor.addBeings(beingToMove);
            beingToMove.forEach(
                    person -> Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + person.getName() + " moved outside")
            );
        }
    }

    private boolean moveOutside(Being being) {
        if (being instanceof Person) {
            return !((Person) being).hasSomeActivity() || ((Person) being).finishedActivity();
        }
        return true;
    }

    private void useAllApplianceInRoom(Room room) {
        for (var person : room.getBeings()) {
            if (personCanUseAppliance(room, person)) {
                doEvent(person);
            }
        }
    }

    private void doEvent(Being being) {
        try {
            Event event = being.getFirstAvailableEvent();
            being.setUsedAppliance(event.getAppliance());
            event.doEvent();
            Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + being.getName() + " do event " + being.getUsedAppliance().getName());
        } catch (UnsupportedEventTypeException e) {
            being.clearFirstEvent();
            Logger.getLogger().logMessage(AbstractLogger.ERROR, "Person " + being.getName() + " cannot do event " + being.getUsedAppliance().getName());
        }

    }

    private void updateOnCycleEnd() {
//        update events
        rooms.stream()
                .map(Room::getBeings)
                .flatMap(List::stream)
                .filter(Being::isUsingAppliance)
                .forEach(person -> {
                    if (person.getFirstAvailableEvent().isDone()) {
                        Logger.getLogger().logMessage(AbstractLogger.INFO, "Person " + person.getName() + " complete event");
                        person.clearFirstEvent();
                    }
                });

//        update activities
        rooms.stream()
                .map(Room::getBeings)
                .flatMap(List::stream)
                .filter(being -> being instanceof Person)
                .map(being -> (Person) being)
                .filter(Person::hasSomeActivity)
                .forEach(Person::clearDoneActivities);

//        update consumptions
        rooms.stream()
                .map(Room::getAppliances)
                .flatMap(List::stream)
                .forEach(Appliance::updateConsumption);
    }

    private boolean personCanUseAppliance(Room room, Being being) {
        return Stream.concat(room.getAppliances().stream(), getAllBlindInRoom(room).stream())
                .anyMatch(appliance -> compareAppliance(being, appliance));
    }

    private List<Appliance<ApplianceState>> getAllBlindInRoom(Room room) {
        return room.getWindows().stream()
                .map(Window::getBlind)
                .toList();
    }

    private boolean compareAppliance(Being being, Appliance appliance) {
        if (appliance == null || being.getFirstAvailableEvent() == null) {
            return false;
        }
        return appliance.equals(being.getFirstAvailableEvent().getAppliance());
    }
}
