package cz.cvut.omo.house.room;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.house.window.Window;
import cz.cvut.omo.persons.Being;
import lombok.Builder;
import lombok.Getter;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Getter
@Builder
public class Room {
    private String id;
    private List<Appliance> appliances;
    private List<Being> beings;
    private List<Window> windows;

    public void addBeings(Collection<Being> arr) {
        Optional.ofNullable(arr)
                .ifPresent(beings::addAll);
    }

    public void removePersons(Collection<Being> arr) {
        beings.removeAll(arr);
    }

    public void addAppliances(Collection<Appliance> arr) {
        Optional.ofNullable(arr)
                .ifPresent(appliances::addAll);
    }

    public void addWindow(Collection<Window> arr) {
        Optional.ofNullable(arr)
                .ifPresent(windows::addAll);
    }
}
