package cz.cvut.omo.house.window;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;
import cz.cvut.omo.appliance.blind.Blind;
import cz.cvut.omo.exceptions.UnsupportedWindowBlindAppliance;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Window {
    private Appliance<ApplianceState> blind;

    public Window(Appliance<ApplianceState> blind) throws UnsupportedWindowBlindAppliance {
        if (blind == null || blind instanceof Blind){
            this.blind = blind;
        } else {
            throw new UnsupportedWindowBlindAppliance();
        }
    }
}
