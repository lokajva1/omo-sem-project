package cz.cvut.omo.exporter;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.omo.house.House;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class HouseExporter {
    public void exportApplianceConsumption(String path) throws IOException {
        House house = House.getInstance();
        ObjectMapper mapper = new ObjectMapper();
        String houseJson = mapper.writeValueAsString(house);
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(houseJson);
        writer.close();
    }
}
