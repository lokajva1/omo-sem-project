package cz.cvut.omo.appliance;

public interface ApplianceState {
    void on(Appliance context);
    void off(Appliance context);
    void running(Appliance context);
    void error(Appliance context);
    void fix(Appliance context);
}
