package cz.cvut.omo.appliance.tv;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;
import lombok.experimental.SuperBuilder;


@SuperBuilder
public class Tv extends Appliance<ApplianceState> {
}
