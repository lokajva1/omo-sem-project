package cz.cvut.omo.appliance.factory;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.blind.Blind;
import cz.cvut.omo.appliance.computer.Computer;
import cz.cvut.omo.appliance.functionality.NewApplianceFunctionalityStrategy;
import cz.cvut.omo.appliance.functionality.OldApplianceFunctionalityStrategy;
import cz.cvut.omo.appliance.radio.Radio;
import cz.cvut.omo.appliance.states.OffState;
import cz.cvut.omo.appliance.tv.Tv;
import cz.cvut.omo.applianceWithContent.audioPlayer.AudioPlayer;
import cz.cvut.omo.applianceWithContent.coffeeMachine.CoffeeMachine;
import cz.cvut.omo.applianceWithContent.fridge.Fridge;
import cz.cvut.omo.applianceWithContent.models.Content;
import cz.cvut.omo.applianceWithContent.washingMachine.WashingMachine;
import cz.cvut.omo.consumption.Consumption;
import cz.cvut.omo.exceptions.UnknownApplianceType;

import java.util.List;

public class ApplianceFactory {
    public Appliance createAppliance(String type, String id, String name, String strategy, int active, int idle, int off) throws UnknownApplianceType {
        return switch (type) {
            case "blind" -> Blind.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new OffState())
                    .build();
            case "computer" -> Computer.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new OffState())
                    .build();
            case "radio" -> Radio.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new OffState())
                    .build();
            case "tv" -> Tv.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new OffState())
                    .build();
            case "audio_player" -> AudioPlayer.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new cz.cvut.omo.applianceWithContent.states.OffState())
                    .content(List.of(new Content("CD")))
                    .build();
            case "coffee_machine" -> CoffeeMachine.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new cz.cvut.omo.applianceWithContent.states.OffState())
                    .content(List.of(new Content("cup")))
                    .build();
            case "fridge" -> Fridge.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new cz.cvut.omo.applianceWithContent.states.OffState())
                    .content(List.of(new Content("food")))
                    .build();
            case "washing_machine" -> WashingMachine.builder()
                    .name(name)
                    .id(id)
                    .functionalityStrategy(strategy.equals("NEW") ? new NewApplianceFunctionalityStrategy() : new OldApplianceFunctionalityStrategy())
                    .consumptionTable(new Consumption(active, idle, off))
                    .functionality(100)
                    .isUsed(false)
                    .manual(null)
                    .state(new cz.cvut.omo.applianceWithContent.states.OffState())
                    .content(List.of())
                    .content(List.of(new Content("shirt")))
                    .build();
            default -> throw new UnknownApplianceType();
        };
    }

}
