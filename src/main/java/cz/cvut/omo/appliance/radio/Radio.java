package cz.cvut.omo.appliance.radio;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class Radio extends Appliance<ApplianceState> {
}
