package cz.cvut.omo.appliance.functionality;

public interface FunctionalityStrategy {
    int reduceFunctionality(int functionality);
}
