package cz.cvut.omo.appliance.functionality;

public class NewApplianceFunctionalityStrategy implements FunctionalityStrategy {
    @Override
    public int reduceFunctionality(int functionality) {
        return functionality - 1;
    }
}
