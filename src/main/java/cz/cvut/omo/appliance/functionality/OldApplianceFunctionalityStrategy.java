package cz.cvut.omo.appliance.functionality;

public class OldApplianceFunctionalityStrategy implements FunctionalityStrategy {
    @Override
    public int reduceFunctionality(int functionality) {
        return functionality - 2;
    }
}
