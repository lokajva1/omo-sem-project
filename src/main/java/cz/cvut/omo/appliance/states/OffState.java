package cz.cvut.omo.appliance.states;

import cz.cvut.omo.appliance.Appliance;

public class OffState extends AbstractApplianceState {
    @Override
    public void on(Appliance context) {
        context.setState(new OnState());
    }
}
