package cz.cvut.omo.appliance.states;

import cz.cvut.omo.appliance.Appliance;

public class ErrorState extends AbstractApplianceState{
    @Override
    public void fix(Appliance context) {
        context.setFunctionality(100);
        context.setState(new OnState());
    }
}
