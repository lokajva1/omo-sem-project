package cz.cvut.omo.appliance.states;

import cz.cvut.omo.appliance.Appliance;

public class RunState extends AbstractApplianceState{
    @Override
    public void on(Appliance context) {
        super.on(context);
        context.setState(new OnState());
    }
}
