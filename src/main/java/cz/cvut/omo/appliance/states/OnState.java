package cz.cvut.omo.appliance.states;

import cz.cvut.omo.appliance.Appliance;

public class OnState extends AbstractApplianceState {

    @Override
    public void running(Appliance context) {
        super.running(context);
        context.setState(new RunState());
    }
}
