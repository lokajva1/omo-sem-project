package cz.cvut.omo.appliance.states;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;

public abstract class AbstractApplianceState implements ApplianceState {

    @Override
    public void on(Appliance context) {
    }

    @Override
    public void off(Appliance context) {
        context.setState(new OffState());
    }

    @Override
    public void running(Appliance context) {
        checkFunctionality(context);
    }

    @Override
    public void error(Appliance context) {

    }

    @Override
    public void fix(Appliance context) {

    }

    protected void checkFunctionality(Appliance context) {
        if (!context.isOperational()) {
            context.setState(new ErrorState());
        }
    }
}
