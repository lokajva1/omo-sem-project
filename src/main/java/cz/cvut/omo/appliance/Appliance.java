package cz.cvut.omo.appliance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.omo.appliance.functionality.FunctionalityStrategy;
import cz.cvut.omo.loader.manual.ApplianceManualLoader;
import cz.cvut.omo.appliance.states.OffState;
import cz.cvut.omo.appliance.states.RunState;
import cz.cvut.omo.consumption.Consumption;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * state pattern
 * strategy pattern
 * @param <T> states for state machine
 */
@Getter
@Setter
@SuperBuilder
public abstract class Appliance<T extends ApplianceState> {
    private String id;

    private String name;
    @JsonIgnore
    private FunctionalityStrategy functionalityStrategy;
    private Consumption consumptionTable;
    private int consumption;
    private int functionality;

    private boolean isUsed;
    @JsonIgnore
    private ApplianceManualLoader applianceManualLoader;
    private String manual;
    @JsonIgnore
    protected T state;
    public void on() {
        state.on(this);
    }
    public void off() {
        state.off(this);
    }
    public void run() {
        state.running(this);
        reduceFunctionality();
    }
    public void error() {
        state.error(this);
        if (applianceManualLoader == null){
            applianceManualLoader = new ApplianceManualLoader();
        }
        manual = applianceManualLoader.getText();
    }
    public void fix() {
        state.fix(this);
    }

    public void reduceFunctionality(){
        functionality = functionalityStrategy.reduceFunctionality(functionality);
        if (functionality < 0){
            functionality = 0;
        }
    }

    public boolean isOperational(){
        return functionality > 0;
    }

    public void updateConsumption(){
        consumption += getRealTimeConsumption();
    }

    private int getRealTimeConsumption(){
        if (state.getClass() == OffState.class){
            return consumptionTable.getOff();
        }
        if (state.getClass() == RunState.class){
            return consumptionTable.getActive();
        }
        return consumptionTable.getIdle();
    }
}
