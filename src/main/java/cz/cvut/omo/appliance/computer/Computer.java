package cz.cvut.omo.appliance.computer;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class Computer extends Appliance<ApplianceState> {

}
