package cz.cvut.omo.appliance.blind;

import cz.cvut.omo.appliance.Appliance;
import cz.cvut.omo.appliance.ApplianceState;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class Blind extends Appliance<ApplianceState>{
}
